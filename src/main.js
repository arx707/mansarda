var React = require("react");
var ReactDOM = require("react-dom");
var Request = require("browser-request");
var Locations = require("./locations.js").locations;

var Mansarda = React.createClass({

  validateUserData: function(){
    return ( this.state.user_email != "" && /(.+)@(.+){2,}\.(.+){2,}/.test(this.state.user_email) && this.state.user_privacy == 1 && this.state.user_policy == 1)
  },

  sendUserData: function(){
    var email = this.state.user_email;
    if(this.state.user_email != 'info@test.com'){
      Request({method:'POST', url:'./api/crm.php', body:email, json:false}, on_response)
      function on_response(er, response, body) {
        if(er)
          console.log(er);
      }      
    }
  },

  formatEuro: function(number){
    var numberStr = parseFloat(number).toFixed(2).toString();
    var numFormatDec = numberStr.slice(-2); /*decimal 00*/
    numberStr = numberStr.substring(0, numberStr.length-3); /*cut last 3 strings*/
    var numFormat = new Array;
    while (numberStr.length > 3) {
      numFormat.unshift(numberStr.slice(-3));
      numberStr = numberStr.substring(0, numberStr.length-3);
    }
    numFormat.unshift(numberStr);
    return numFormat.join('.'); /*format 000.000.000 */
  },


  getPrice: function(){

    var json = {
      "affaccio": (this.state.affaccio == 0) ? "1" : "2",
      "copertura": (this.state.copertura == 0) ? "1" : "2",
      "m-lev": String(this.state["m-lev"]),
      "rinforzo":  (this.state.rinforzo == 0) ? "1" : "2",
      "m-scala": String(this.state["m-scala"]),
      "mq": String(this.state.mq),
      "provincia": String(this.state.provincia),
      "ub":{
        "comune": String(this.state.ub_comune), 
        "zone": (this.state.ub_zone == 0) ? "1" : "2",
        "centro": (this.state.ub_centro == 0) ? "1" : "2",
        "ztl": (this.state.ub_ztl == 0) ? "1" : "2",
        "carico": (this.state.ub_carico == 0) ? "1" : "2",
        "isole": (this.state.ub_isole == 0) ? "1" : "2",
        "piano": (this.state.ub_piano == 0) ? "1" : "2"
      },
      "level": this.state.level,
      "radio":{
        "DEMOLIZIONE": String(this.state.radio_DEMOLIZIONE),
        "TRAMEZZI": String(this.state.radio_TRAMEZZI),
        "INTONACI": String(this.state.radio_INTONACI),
        "ELETTRICO": String(this.state.radio_ELETTRICO),
        "IDRICO": String(this.state.radio_IDRICO), 
        "RISCALDAMENTO": String(this.state.radio_RISCALDAMENTO),
        "CONDIZIONAMENTO": String(this.state.radio_CONDIZIONAMENTO),
        "ALLARME": String(this.state.radio_ALLARME),
        "WC-PADRONALE": String(this.state["exc_WC-PADRONALE"]),
        "WC-SERVIZIO": String(this.state["exc_WC-SERVIZIO"]),
        "PAVIMENTI": String(this.state["exc_PAVIMENTI"]),
        "INFISSI-EXT": String(this.state["radio_INFISSI-EXT"]),
        "PERSIANE": String(this.state["radio_PERSIANE"]),
        "INFISSI-INT": String(this.state["radio_INFISSI-INT"]),
        "PITTURE": String(this.state.radio_PITTURE)
      },
      "exc":{
        "WC-PADRONALE": String(this.state["exc_WC-PADRONALE"]),
        "WC-SERVIZIO": String(this.state["exc_WC-SERVIZIO"]),
        "PAVIMENTI": String(this.state["exc_PAVIMENTI"])
      }
    };

    var jsonEncoded = encodeURIComponent(JSON.stringify(json));

    Request('./api/price.php/?dati='+jsonEncoded, function(er, response, body) {
      if(er)
        throw er;
      var data = JSON.parse(body);
      this.setState({
        result: {
          total: data["prezzo-tot"],
          mq: data["prezzo-mq"],
          tips: {
            "DEMOLIZIONE": (data["DEMOLIZIONE"]) ? parseInt(data["DEMOLIZIONE"].prezzo.replace(".","")) : 0,
            "TRAMEZZI": (data["TRAMEZZI"]) ? parseInt(data["TRAMEZZI"].prezzo.replace(".","")) : 0,
            "ELETTRICO": (data["ELETTRICO"]) ? parseInt(data["ELETTRICO"].prezzo.replace(".","")) : 0,
            "IDRICO":  (data["IDRICO"]) ? parseInt(data["IDRICO"].prezzo.replace(".","")) : 0,
            "RISCALDAMENTO": (data["RISCALDAMENTO"]) ? parseInt(data["RISCALDAMENTO"].prezzo.replace(".","")) : 0,
            "CONDIZIONAMENTO": (data["CONDIZIONAMENTO"]) ? parseInt(data["CONDIZIONAMENTO"].prezzo.replace(".","")) : 0,
            "WC-PADRONALE": (data["WC-PADRONALE"]) ? parseInt(data["WC-PADRONALE"].prezzo.replace(".","")) : 0,
            "ALLARME":  (data["ALLARME"]) ? parseInt(data["ALLARME"].prezzo.replace(".","")) : 0,
            "WC-SERVIZIO": (data["WC-SERVIZIO"]) ? parseInt(data["WC-SERVIZIO"].prezzo.replace(".","")) : 0,
            "PAVIMENTI":  (data["PAVIMENTI"]) ? parseInt(data["PAVIMENTI"].prezzo.replace(".","")) : 0,
            "INFISSI-EXT":  (data["INFISSI-EXT"]) ? parseInt(data["INFISSI-EXT"].prezzo.replace(".","")) : 0,
            "PERSIANE": (data["PERSIANE"]) ? parseInt(data["PERSIANE"].prezzo.replace(".","")) : 0,
            "INFISSI-INT": (data["INFISSI-INT"]) ? parseInt(data["INFISSI-INT"].prezzo.replace(".","")) : 0,
            "PITTURE": (data["PITTURE"]) ? parseInt(data["PITTURE"].prezzo.replace(".","")) : 0,
            "SCALA": (data["SCALA"]) ? parseInt(data["SCALA"].prezzo.replace(".","")) : 0
          }
        }
      });
    }.bind(this));

  },

  getLevel: function(level){

      this.setState({
        popup: null
      });

      var qualityLevels = ['null','low','medium','comfort'];
      var regione = Locations.getIDRegione(this.state.provincia);
      var qualityLevel = qualityLevels[level];

      // this.setState({
      //   quality: this.state.qualityQuery
      // });

      if(level >= 1){

        var json = {
          "idr": regione,
          "type-g": qualityLevel,
          "affaccio": (this.state.affaccio == 0) ? "1" : "2",
          "copertura": (this.state.copertura == 0) ? "1" : "2",
          "m-lev": String(this.state["m-lev"]),
          "rinforzo":  (this.state.rinforzo == 0) ? "1" : "2",
          "m-scala": String(this.state["m-scala"]),
          "mq": String(this.state.mq),
          "provincia": String(this.state.provincia),
          "ub":{
            "comune": String(this.state.ub_comune), 
            "zone": (this.state.ub_zone == 0) ? "1" : "2",
            "centro": (this.state.ub_centro == 0) ? "1" : "2",
            "ztl": (this.state.ub_ztl == 0) ? "1" : "2",
            "carico": (this.state.ub_carico == 0) ? "1" : "2",
            "isole": (this.state.ub_isole == 0) ? "1" : "2",
            "piano": (this.state.ub_piano == 0) ? "1" : "2"
          },
          "level": this.state.level
        };

        var jsonEncoded = encodeURIComponent(JSON.stringify(json));

        Request('./api/level.php/?dati='+jsonEncoded, function(er, response, body) {
          if(er)
            throw er;
          var data = JSON.parse(body);
          var qualityTips = [];

          var green = data['GREEN'];
          var comfort = data['COMFORT'];

          if(green){
            for (var greenItem in green) {
              var _greenItem = green[greenItem];
              _greenItem.prezzo = this.formatEuro(_greenItem.prezzo);
              qualityTips.push(_greenItem);
            }
          }

          if(comfort){
            for (var comfortItem in comfort) {
              var _comfortItem = comfort[comfortItem];
              _comfortItem.prezzo = this.formatEuro(_comfortItem.prezzo);
              qualityTips.push(_comfortItem);
            }
          }

          this.setState({
            qualityTips: qualityTips,
            qualitySave: data['RISPARMIO']
          });


        }.bind(this));

      }

      else{
        this.setState({
          qualityTips: [],
          qualitySave: 0
        });
      }

  },

  getPriceStats: function(){

    var total = this.state.result.total;
    var levels = {};

    var totalComputed = 0;
    var mqComputed = 0;

    var muratura = this.state.result.tips["DEMOLIZIONE"] + this.state.result.tips["TRAMEZZI"] + this.state.result.tips["SCALA"];
    var elettrico = this.state.result.tips["ELETTRICO"];
    var clima = this.state.result.tips["RISCALDAMENTO"] + this.state.result.tips["CONDIZIONAMENTO"];
    var wc = this.state.result.tips["WC-SERVIZIO"] + this.state.result.tips["WC-PADRONALE"];
    var pavimenti = this.state.result.tips["PAVIMENTI"];
    var infissi = this.state.result.tips["INFISSI-EXT"] + this.state.result.tips["INFISSI-INT"] + this.state.result.tips["PERSIANE"];
    var altro = this.state.result.tips["IDRICO"] + this.state.result.tips["ALLARME"] + this.state.result.tips["PITTURE"];

    var qualityLightTip = "";
    var qualityLightPriceMQ = 0;
    var qualityTotal = 0;
    var qualityTips = this.state.qualityTips;

    if(this.state.quality == 1){
      qualityLightTip = "Finestre per tetti più grandi";
      qualityLightPriceMQ = 5;
    }
    else if(this.state.quality == 2){
      qualityLightTip = "Finestre per tetti domotiche";
      qualityLightPriceMQ = 10;
    }
    else if(this.state.quality == 3){
      qualityLightTip = "Finestre per tetti ad elevate prestazioni";
      qualityLightPriceMQ = 15;
    }

    for(var tip in qualityTips){
      qualityTotal += parseInt(qualityTips[tip].prezzo.replace(".",""));
    }


    if(total == null){
      total = 0;
      mq = 0;
      levels = {
        muratura: 0,
        elettrico: 0,
        clima: 0,
        wc: 0,
        pavimenti: 0,
        infissi: 0,
        altro: 0
      }
    }

    else{
      total = parseInt(this.state.result.total.replace(".",""));

      levels = {
        muratura:  Math.round( (muratura / total * 100) / 10) * 10,
        elettrico: Math.round( (elettrico / total * 100) / 10) * 10,
        clima: Math.round( (clima / total * 100) / 10) * 10,
        wc: Math.round( (wc / total * 100) / 10) * 10        ,
        pavimenti: Math.round( (pavimenti / total * 100) / 10) * 10,
        infissi: Math.round( (infissi / total * 100) / 10) * 10,
        altro: Math.round( (altro / total * 100) / 10) * 10
      }
    }

    return {
      levels: levels,
      total:  this.formatEuro( (total + parseInt(qualityTotal) + parseInt(this.state.mq * qualityLightPriceMQ)) ),
      mq:  this.formatEuro( parseInt( (total + parseInt(qualityTotal)) / this.state.mq ) + qualityLightPriceMQ ),
      light: {
        tip: qualityLightTip,
        price: this.formatEuro(parseInt(this.state.mq * qualityLightPriceMQ))
      }
    };

  },

  setStepResults: function(){

    if(this.state.popup != 'computing'){

      this.setState({
        popup:'computing'
      });

      var timer = setInterval(function(){
        var index = this.state.computingIndex;
        if(index < 4){
          this.setState({
            computingIndex: (index+1)
          });          
        }
        else{
          clearInterval(timer);

          if( this.validateUserData() == false){
            if(this.state.popup != 'email'){
              this.setState({
                popup:'email'
              });
            }
          }
          else{
            this.getPrice();
            this.sendUserData();
            this.setState({
              stepIndex: 7,
              popup: false
            });
          }

        }

      }.bind(this),2000);


    }


  },

  setStepResultsAfterEmail: function(){

    if( this.validateUserData() == false){
      if(this.state.popup != 'email'){
        this.setState({
          popup:'email'
        });
      }
    }
    else{
      this.getPrice();
      this.sendUserData();
      this.setState({
        stepIndex: 7,
        popup: false
      });
    }

  },

  setStepRestart: function(){
    this.setState({
      stepIndex: 0,
      computingIndex: 0,
      qualitySection: 'intro',
      quality: 0,
      qualityQuery: 0,
      qualityTips: [],
      qualitySave: 0,

      mq: 100,

      provincia:77,
      ub_comune: 0,

      ub_zone:0,
      ub_centro:0,
      ub_ztl:0,
      ub_carico:0,
      ub_isole:0,
      ub_piano:0,

      affaccio:0,
      copertura:0,

      "radio_INFISSI-EXT":0,
      "radio_PERSIANE":0,
      "radio_INFISSI-INT":0,

      radio_DEMOLIZIONE:0,
      radio_INTONACI:0,
      radio_ELETTRICO:0,
      radio_IDRICO:0,
      radio_RISCALDAMENTO:0,
      radio_CONDIZIONAMENTO:0,
      radio_ALLARME:0,
      radio_TRAMEZZI:0,
      "exc_WC-PADRONALE":0,
      "exc_WC-SERVIZIO":0,
      exc_PAVIMENTI:0,
      radio_PITTURE:0,
      rinforzo:0,

      "m-lev":1,        
      "m-scala":0,
      "level":"high",

      result: {
        total: null,
        mq: 100,
        tips: {
          "DEMOLIZIONE": 0,
          "TRAMEZZI": 0,
          "ELETTRICO": 0,
          "IDRICO":  0,
          "RISCALDAMENTO": 0,
          "CONDIZIONAMENTO": 0,
          "WC-PADRONALE": 0,
          "ALLARME": 0,
          "WC-SERVIZIO": 0,
          "PAVIMENTI": 0,
          "INFISSI-EXT": 0,
          "PERSIANE": 0,
          "INFISSI-INT": 0,
          "PITTURE": 0,
          "SCALA": 0
        }
      }
    });
  },

  setStepIndexAbsolute: function(newIndex){

    var legal = true;
    var currentIndex = this.state.stepIndex;

    // validations

    if(newIndex >= 3 && this.state.ub_comune == 0){
      legal = false;
    }

    if(legal == true){
      this.setState({
        stepIndex: newIndex
      })
    }

  },

  setStepIndex: function(modifier){
    this.setStepIndexAbsolute(this.state.stepIndex + modifier);
  },

  setSectionResult: function(section){
    this.setState({
      qualitySection: section
    });
  },

  setProvincia: function(event){
    var value = event.target.value;
    if(value != this.state.provincia){
      this.setState({
        provincia: value
      });
    }
  },

  setFormDataFromEvent: function(label,event){
    var value = event.target.value;
    var newState = {};
    if(value != this.state[label]){
      newState[label] = value;
      this.setState(newState);
    }
  },

  setFormDataFromSelect: function(label,event){
    var value = event.target.value;
    var newState = {};
    if(value != this.state[label]){
      newState[label] = value;
      this.setState(newState);
    }
    // event.target.blur();
  },

  setFormDataFromMulti: function(event){
    var options = event.target.options;
    var newState = {};
    for (var i = 0, l = options.length; i < l; i++) {
      if (options[i].selected) {
        newState[options[i].value] = 1;
      }
      else{
        newState[options[i].value]= 0;
      }
    }
    this.setState(newState);
  },

  getFormDataFromMulti: function(values){
    var array = [];
    for (var i = 0; i < values.length; i++) {
      var valueLabel = values[i];
      var state = this.state[valueLabel];
      if(state == 1){
        array.push(valueLabel);
      }
    }
    return array;
  },

  setFormData: function(value,label){
    var newState = {};
    newState[label] = value;
    this.setState(newState);
  },

  setQuality: function(value){
    this.getLevel(value);
    this.setState({
      quality: value,
      qualitySection: 'final'
    });
  },

  toggleFormData: function(label,event){
    var oldState = this.state[label];
    var value = 1 - (this.state[label]);
    var newState = {};
    newState[label] = value;
    this.setState(newState);
  },

  openPopup: function(popup){
    this.setState({
      popup: popup
    });
  },

  sendMail: function(){
    if ( this.state.user_email != "" && /(.+)@(.+){2,}\.(.+){2,}/.test(this.state.user_email)){

      this.setState({
        popup: false}
      );

      var json = this.getPriceStats();
      var jsonEncoded = encodeURIComponent(JSON.stringify(json));
      var json2 = this.state.qualityTips;
      var jsonEncoded2 = encodeURIComponent(JSON.stringify(json2));

        Request('./api/mail.php/?dati='+jsonEncoded+'&tips='+jsonEncoded2+'&email='+this.state.user_email, function(er, response, body) {
          if(er)
            throw er;
        });
    }
  },

  getInitialState: function(){
    return({
      stepIndex: 0,
      computingIndex: 0,
      qualitySection: 'intro',
      loadingState: 0,
      popup: null,
      qualityQuery: 0,

      user_email: "",
      user_privacy: 1,
      user_policy: 1,

      mq: 100,

      provincia:77,
      ub_comune: 0,

      ub_zone:0,
      ub_centro:0,
      ub_ztl:0,
      ub_carico:0,
      ub_isole:0,
      ub_piano:0,

      affaccio:0,
      copertura:0,

      "radio_INFISSI-EXT":0,
      "radio_PERSIANE":0,
      "radio_INFISSI-INT":0,

      radio_DEMOLIZIONE:0,
      radio_INTONACI:0,
      radio_ELETTRICO:0,
      radio_IDRICO:0,
      radio_RISCALDAMENTO:0,
      radio_CONDIZIONAMENTO:0,
      radio_ALLARME:0,
      radio_TRAMEZZI:0,
      "exc_WC-PADRONALE":0,
      "exc_WC-SERVIZIO":0,
      exc_PAVIMENTI:0,
      radio_PITTURE:0,
      rinforzo:0,

      "m-lev":1,        
      "m-scala":0,
      "level":"high",

      quality: 0,
      qualityTips: [],
      qualitySave: 0,

      result: {
        total: null,
        mq: null,
        tips: {
          "DEMOLIZIONE": 0,
          "TRAMEZZI": 0,
          "ELETTRICO": 0,
          "IDRICO":  0,
          "RISCALDAMENTO": 0,
          "CONDIZIONAMENTO": 0,
          "WC-PADRONALE": 0,
          "ALLARME": 0,
          "WC-SERVIZIO": 0,
          "PAVIMENTI": 0,
          "INFISSI-EXT": 0,
          "PERSIANE": 0,
          "INFISSI-INT": 0,
          "PITTURE": 0,
          "SCALA": 0
        }
      },
    });
  },

  componentWillMount: function(){
    this.steps = [1,2,3,4,5,6,7];
    this.provinces = Locations.getProvincie();
    this.strings = {
      avanti: "avanti",
      indietro: "indietro",
      quality: ['Normale','Media','Buona','Ottima']
    }
    this.computeTime = 2000;
    this.qualityStrings = [
      {title:"Nessun investimento",text: "Mantieni le stesse impostazioni che hai selezionato; le cifre calcolate rimarranno inviariate."},
      {title:"investimento base",text: "interventi limitati che generano un moderato risparmio energetico."},
      {title:"investimento avanzato",text: "interventi agli impianti di muratura ed isolamento che assicurano un maggior risparmio energetico."},
      {title:"investimento completo",text: "il maggior numero di interventi e la maggiore qualità di investimento."}
    ];
  },

  render: function(){

    // general state classes
    var stepIndexClass = "mansarda_index" + this.state.stepIndex;
    var loadingStateClass = this.state.loadingState === true ? 'mansarda_loading' : 'mansarda_loading_false';
    var helpStateClass = this.state.helpState === true ? 'mansarda_help' : 'mansarda_help_false';
    var popupClass = 'mansarda_popup_inactive';
    if( this.state.popup != false ){
      popupClass = "mansarda_popup_" + this.state.popup + "_active";
    }

    var appClass = "mansarda_wrap " + stepIndexClass + " " + loadingStateClass + " " + helpStateClass + " " + popupClass;

    // single steps state classes
    var cityClass = "mansarda_city_selected" + this.state.ub_comune;
    var doorsIntCLass = this.state["radio_INFISSI-INT"] == 1 ? "mansarda_doors_selected" : "";
    var doorsPerCLass = this.state["radio_PERSIANE"] == 1 ? "mansarda_doors_selected" : "";
    var doorsExtCLass = this.state["radio_INFISSI-EXT"] == 1 ? "mansarda_doors_selected" : "";
    
    var sizeMQ = (this.state.mq > 350) ? 350 : this.state.mq;
    var sizePercent = parseInt( ((sizeMQ*0.25) + 75) * 0.45);
    var sizeString = sizePercent + "%";
    var overlay1visible = sizeMQ <= 99 ? '' : 'mansarda_transparent';
    var overlay2visible = (sizeMQ >= 100 && sizeMQ <= 180) ? '' : 'mansarda_transparent';
    var overlay3visible = sizeMQ >= 181 ? '' : 'mansarda_transparent';

    var placeSelection = this.getFormDataFromMulti(["ub_zone","ub_centro","ub_isole","ub_ztl","ub_carico",,"ub_piano","affaccio","copertura"]);
    var implantsSelection = this.getFormDataFromMulti(["radio_DEMOLIZIONE","radio_TRAMEZZI","exc_PAVIMENTI","rinforzo","radio_PITTURE","radio_INTONACI","radio_ELETTRICO","radio_IDRICO","radio_RISCALDAMENTO","radio_CONDIZIONAMENTO","radio_ALLARME","exc_WC-PADRONALE"]);
    var resultStats = this.getPriceStats();
    var resultPriceTotal = (resultStats && resultStats.total) ? "€ " + resultStats.total : 'Carico..';
    var resultPriceMQ = (resultStats && resultStats.mq) ? "€ " + resultStats.mq : 'Carico..';
    var qualityTitle = this.qualityStrings[this.state.quality].title;
    var qualityText = this.qualityStrings[this.state.quality].text;


    return (
      <div className={appClass}>

        <div className="mansarda_popup_email mansarda_popup">
          <div className="mansarda_popup_back" onClick={this.openPopup.bind(this,false)}></div>
          <div className="mansarda_popup_content">
            <h2>Inserisci il tuo indirizzo<br />email per proseguire</h2>
            <input className="mansarda_popup_text" type="text" value={this.state.user_email} onChange={this.setFormDataFromEvent.bind(this,'user_email')}></input>
            <button className="mansarda_popup_submit" onClick={this.setStepResultsAfterEmail}>Invia</button>
            <div className="mansarda_popup_info">
              <label><input type="checkbox" checked={this.state.user_privacy} onChange={this.toggleFormData.bind(this,'user_privacy')}></input>Acconsento al <a href="http://www.mansarda.it/privacy-policy/" target="_BLANK">trattamento dei dati personali</a></label><br/>
              <label><input type="checkbox" checked={this.state.user_policy} onChange={this.toggleFormData.bind(this,'user_policy')}></input>Ho letto e accetto le <a href="http://www.mansarda.it/condizioni-del-servizio-preventivo-mansarda/" target="_BLANK">condizioni del servizio</a></label>
            </div>
            <button className="mansarda_popup_close" onClick={this.openPopup.bind(this,false)}><i className="icon-cross"></i></button>
          </div>
        </div>

        <div className="mansarda_popup_computing mansarda_popup">
          <div className="mansarda_popup_back"></div>
          <div className="mansarda_popup_content">
            <h2>Stiamo calcolando i risultati<br />sulla base dei dati inseriti..</h2>
            <div className="mansarda_popup_info">
              <div className={this.state.computingIndex >= 1 ? "mansarda_popup_computed mansarda_popup_computed_active" : "mansarda_popup_computed"}>
                <div className="mansarda_popup_computed_img"></div>
                <h3>elaborazione località</h3>
              </div>
              <div className={this.state.computingIndex >= 2 ? "mansarda_popup_computed mansarda_popup_computed_active" : "mansarda_popup_computed"}>
                <div className="mansarda_popup_computed_img"></div>
                <h3>analisi dati mansarda</h3>
              </div>
              <div className={this.state.computingIndex >= 3 ? "mansarda_popup_computed mansarda_popup_computed_active" : "mansarda_popup_computed"}>
                <div className="mansarda_popup_computed_img"></div>
                <h3>analisi interventi</h3>
              </div>
              <div className={this.state.computingIndex >= 4 ? "mansarda_popup_computed mansarda_popup_computed_active" : "mansarda_popup_computed"}>
                <div className="mansarda_popup_computed_img"></div>
                <h3>elaborazione statistiche</h3>
              </div>

            </div>
          </div>
        </div>

        <div className="mansarda_popup_send mansarda_popup">
          <div className="mansarda_popup_back" onClick={this.openPopup.bind(this,false)}></div>
          <div className="mansarda_popup_content">
            <h2>Inserisci l'indirizzo<br />del destinatario</h2>
            <input className="mansarda_popup_text" type="text" value={this.state.user_email} onChange={this.setFormDataFromEvent.bind(this,'user_email')}></input>
            <button className="mansarda_popup_submit" onClick={this.sendMail}>Invia</button>
            <button className="mansarda_popup_close" onClick={this.openPopup.bind(this,false)}><i className="icon-cross"></i></button>
          </div>
        </div>

        <div className="mansarda_popup_quality mansarda_popup">
          <div className="mansarda_popup_back" onClick={this.openPopup.bind(this,false)}></div>
          <div className="mansarda_popup_content">
            <h2>Sposta il cursore per ottenere<br />suggerimenti sull'investimento</h2>
            <button className="mansarda_popup_close" onClick={this.openPopup.bind(this,false)}><i className="icon-cross"></i></button>
            <div className="mansarda_popup_controls">
              <input 
                type="range" 
                className="mansarda_popup_range" 
                value={this.state.qualityQuery}
                min="0"
                max="3"
                step="1"
                onChange={this.setFormDataFromEvent.bind(this,'qualityQuery')}></input>
            </div>
            <button className="mansarda_popup_submit" onClick={this.getLevel}>Avanti</button>
          </div>
        </div>

        <div className="mansarda_steps">

          <div className="mansarda_step mansarda_splash mansarda_step0">
            <div className="mansarda_splash_city"></div>
            <img className="mansarda_splash_logo" src="images/logo.png"/>
            <p className="mansarda_splash_text">Scopri le opportunita che che può darti la ristrutturazione della tua vecchia mansarda grazie al nostro configuratore interattivo.</p>

            <button className="mansarda_splash_go" onClick={this.setStepIndex.bind(this,1)}>Inizia</button>
          </div>

          <div className="mansarda_step mansarda_mq mansarda_step1">

            <div className="mansarda_half">

              <h2>Inserisci i metri quadri<br />della tua mansarda</h2>

              <input 
                type="text" 
                value={this.state.mq} 
                className="mansarda_mq_text"
                onChange={this.setFormDataFromEvent.bind(this,'mq')}></input>

              <input 
                type="range" 
                name="mansarda_mq_range" 
                className="mansarda_mq_range" 
                min="2" 
                max="200" 
                step="1"
                value={this.state.mq}
                onChange={this.setFormDataFromEvent.bind(this,'mq')}></input>

              <div className="mansarda_navigation">
                <button className="mansarda_navigation_prev" onClick={this.setStepIndex.bind(this,-1)}>{this.strings.indietro}</button>
                <button className="mansarda_navigation_next" onClick={this.setStepIndex.bind(this,1)}>{this.strings.avanti}</button>
              </div>

              <div className="mansarda_dots">
                {this.steps.map(function(step, i){
                  return <button key={i} className={i == this.state.stepIndex ? 'active' : 'inactive'} onClick={this.setStepIndexAbsolute.bind(this,i)}></button>
                }.bind(this))}
              </div>

            </div>

            <div className="mansarda_half2">
              <div className="mansarda_art_back"></div>
              <img src="images/home.svg" className="mansarda_art_home" style={{width:sizeString}}/>
              <img src="images/home_overlay1.svg" className={"mansarda_art_home_overlay1 " + overlay1visible} style={{width:sizeString}}/>
              <img src="images/home_overlay2.svg" className={"mansarda_art_home_overlay2 " + overlay2visible} style={{width:sizeString}}/>
              <img src="images/home_overlay3.svg" className={"mansarda_art_home_overlay3 " + overlay3visible} style={{width:sizeString}}/>
              <img className="mansarda_art_tree1" src="images/tree.svg" />
              <img className="mansarda_art_tree2" src="images/tree.svg" />
            </div>

          </div>

          <div className={"mansarda_step mansarda_place_city mansarda_step2 " + cityClass}>

            <div className="mansarda_full">
              <h2>Quanto é grande<br />il tuo comune?</h2>

              <div className="mansarda_city_icons">
                <div className="mansarda_city_icon mansarda_city_size1" onClick={this.setFormData.bind(this,1,'ub_comune')}>
                  <img src="images/city1.svg" />
                  <p>Meno di 15.000<br />abitanti</p>
                </div>
                <div className="mansarda_city_icon mansarda_city_size2" onClick={this.setFormData.bind(this,2,'ub_comune')}>
                  <img src="images/city2.svg" />
                  <p>15.000 - 100.000<br />abitanti</p>
                </div>
                <div className="mansarda_city_icon mansarda_city_size3" onClick={this.setFormData.bind(this,3,'ub_comune')}>
                  <img src="images/city3.svg" />
                  <p>100.000 - 400.000<br />abitanti</p>
                </div>
                <div className="mansarda_city_icon mansarda_city_size4" onClick={this.setFormData.bind(this,4,'ub_comune')}>
                  <img src="images/city4.svg" />
                  <p>Oltre i 400.000<br />abitanti</p>
                </div>
              </div>

            </div>

            <div className="mansarda_navigation">
              <button className="mansarda_navigation_prev" onClick={this.setStepIndex.bind(this,-1)}>{this.strings.indietro}</button>
              <button className="mansarda_navigation_next" onClick={this.setStepIndex.bind(this,1)}>{this.strings.avanti}</button>
            </div>

            <div className="mansarda_dots">
              {this.steps.map(function(step, i){
                return <button key={i} className={i == this.state.stepIndex ? 'active' : 'inactive'} onClick={this.setStepIndexAbsolute.bind(this,i)}></button>
              }.bind(this))}
            </div>

          </div>

          <div className="mansarda_step mansarda_place mansarda_step3">

            <div className="mansarda_half">

              <h2>Seleziona la provincia in cui<br />si trova la mansarda</h2>

              <select onChange={this.setFormDataFromSelect.bind(this,'provincia')} className="mansarda_place_province">
                <optgroup>
                {this.provinces.map(function(province, i){
                  return <option key={i} value={province.id}>{province.name}</option>
                }.bind(this))}
                </optgroup>
              </select>

            </div>

            <div className="mansarda_half2">

              <h2>Clicca sulle voci <br />per le informazioni aggiuntive</h2>

              <select 
                multiple={true}
                className="mandarda_place_advanced_mobile" 
                onChange={this.setFormDataFromMulti}
                value={placeSelection}>
                <option value="ub_zone">Zona o quartiere di pregio</option>
                <option value="ub_centro">Ubicazione in Centro storico</option>
                <option value="ub_isole">Isole minori</option>
                <option value="ub_ztl">Traffico limitato (ZTL, Ecopass, ecc)</option>
                <option value="ub_carico">Limitazioni orarie carico scarico</option>
                <option value="ub_piano">Non al primo piano (secondo o più)</option>
                <option value="affaccio">Affaccio su strada per ponteggi</option>
                <option value="copertura">Copertura già coibentata</option>
              </select>

              <div className="mansarda_place_advanced">
                <label>
                  <input type="checkbox" checked={this.state.ub_zone} onChange={this.toggleFormData.bind(this,'ub_zone')}></input>
                  <em>Zona o quartiere<br />di pregio</em>
                </label>
                <label>
                  <input type="checkbox" checked={this.state.ub_centro} onChange={this.toggleFormData.bind(this,'ub_centro')}></input>
                  <em>Ubicazione in<br />Centro storico</em>
                </label>
                <label>
                  <input type="checkbox" checked={this.state.ub_isole} onChange={this.toggleFormData.bind(this,'ub_isole')}></input>
                  <em>Isole minori</em>
                </label>
                <label>
                  <input type="checkbox" checked={this.state.ub_ztl} onChange={this.toggleFormData.bind(this,'ub_ztl')}></input>
                  <em>Traffico limitato<br />(ZTL, Ecopass, ecc)</em>
                </label>
                <label>
                  <input type="checkbox" checked={this.state.ub_carico} onChange={this.toggleFormData.bind(this,'ub_carico')}></input>
                  <em>Limitazioni orarie<br />carico scarico</em>
                </label>
                <label>
                  <input type="checkbox" checked={this.state.ud_piano} onChange={this.toggleFormData.bind(this,'ub_piano')}></input>
                  <em>Non al primo piano (secondo o più)</em>
                </label>
                <label>
                  <input type="checkbox" checked={this.state.affaccio} onChange={this.toggleFormData.bind(this,'affaccio')}></input>
                  <em>Affaccio su strada per ponteggi</em>
                </label>
                <label>
                  <input type="checkbox" checked={this.state.copertura} onChange={this.toggleFormData.bind(this,'copertura')}></input>
                  <em>Copertura già coibentata</em>
                </label>

              </div>

            </div>

            <div className="mansarda_navigation">
              <button className="mansarda_navigation_prev" onClick={this.setStepIndex.bind(this,-1)}>{this.strings.indietro}</button>
              <button className="mansarda_navigation_next" onClick={this.setStepIndex.bind(this,1)}>{this.strings.avanti}</button>
            </div>

            <div className="mansarda_dots">
              {this.steps.map(function(step, i){
                return <button key={i} className={i == this.state.stepIndex ? 'active' : 'inactive'} onClick={this.setStepIndexAbsolute.bind(this,i)}></button>
              }.bind(this))}
            </div>

          </div>

          <div className="mansarda_step mansarda_doors mansarda_step4">

            <div className="mansarda_full">
              <h2>Vuoi rinnovare le finestre?<br />Clicca sugli infissi che hai scelto</h2>

              <div className="mansarda_doors_icons">
                <div className={"mansarda_doors_icon mansarda_door_infissi_int " + doorsIntCLass} onClick={this.toggleFormData.bind(this,'radio_INFISSI-INT')}>
                  <img src="images/door2.svg" />
                  <p>Finestre per tetti</p>
                </div>
                <div className={"mansarda_doors_icon mansarda_door_persiane " + doorsPerCLass} onClick={this.toggleFormData.bind(this,'radio_PERSIANE')}>
                  <img src="images/door1.svg" />
                  <p>Tapparelle e tende per finestre per tetti</p>
                </div>
              </div>

            </div>

            <div className="mansarda_navigation">
              <button className="mansarda_navigation_prev" onClick={this.setStepIndex.bind(this,-1)}>{this.strings.indietro}</button>
              <button className="mansarda_navigation_next" onClick={this.setStepIndex.bind(this,1)}>{this.strings.avanti}</button>
            </div>

            <div className="mansarda_dots">
              {this.steps.map(function(step, i){
                return <button key={i} className={i == this.state.stepIndex ? 'active' : 'inactive'} onClick={this.setStepIndexAbsolute.bind(this,i)}></button>
              }.bind(this))}
            </div>

          </div>

          <div className="mansarda_step mansarda_implants mansarda_step5">

            <div className="mansarda_half">

              <h2>Seleziona il livello di pregio<br />delle finiture</h2>

              <select onChange={this.setFormDataFromSelect.bind(this,"level")} className="mansarda_implants_level" value={this.state["level"]}>
                <option value="low">Basso</option>
                <option value="medium">Medio</option>
                <option value="high">Alto</option>
              </select>

              <p className="mansarda_implants_info">
                Scegli il livello di rifinitura degli interventi e qualità dei materiali; questo parametro, assieme al numero di interventi, influenzerà il calcolo del costo finale.
              </p>

            </div>

            <div className="mansarda_half2">

              <h2>Clicca sugli interventi<br />che vuoi operare</h2>

              <select multiple="multiple" className="mandarda_implants_advanced_mobile" onChange={this.setFormDataFromMulti} value={this.state.placeSelection}>
                <option value="radio_DEMOLIZIONE">Modifica pareti interne</option>
                <option value="radio_TRAMEZZI">Piccole murature</option>
                <option value="exc_PAVIMENTI">Rifacimento pavimenti</option>
                <option value="rinforzo">Rinforzo soletta calpestio</option>
                <option value="radio_PITTURE">Ridipingere</option>
                <option value="radio_INTONACI">Intonaci</option>
                <option value="radio_ELETTRICO">Impianto elettrico</option>
                <option value="radio_IDRICO">Impianto idrico</option>
                <option value="radio_RISCALDAMENTO">Impianto riscaldamento</option>
                <option value="radio_CONDIZIONAMENTO">Non al primo piano (secondo o più)</option>
                <option value="radio_ALLARME">Sistema allarme</option>
                <option value="exc_WC-PADRONALE">Rifacimento bagno</option>
              </select>

              <div className="mansarda_implants_advanced">
                <label className="mansarda_implants_walls">
                  <input type="checkbox" checked={this.state.radio_DEMOLIZIONE} onChange={this.toggleFormData.bind(this,'radio_DEMOLIZIONE')}></input>
                  <em>Modifica pareti interne</em>
                </label>
                <label className="mansarda_implants_wallsmini">
                  <input type="checkbox" checked={this.state.radio_TRAMEZZI} onChange={this.toggleFormData.bind(this,'radio_TRAMEZZI')}></input>
                  <em>Piccole murature</em>
                </label>
                <label className="mansarda_implants_floor">
                  <input type="checkbox" checked={this.state.exc_PAVIMENTI} onChange={this.toggleFormData.bind(this,'exc_PAVIMENTI')}></input>
                  <em>Rifacimento pavimenti</em>
                </label>
                <label className="mansarda_implants_floor2">
                  <input type="checkbox" checked={this.state.rinforzo} onChange={this.toggleFormData.bind(this,'rinforzo')}></input>
                  <em>Rinforzo soletta calpestio</em>
                </label>
                <label  className="mansarda_implants_paint">
                  <input type="checkbox" checked={this.state.radio_PITTURE} onChange={this.toggleFormData.bind(this,'radio_PITTURE')}></input>
                  <em>Ridipingere</em>
                </label>
                <label className="mansarda_implants_plaster">
                  <input type="checkbox" checked={this.state.radio_INTONACI} onChange={this.toggleFormData.bind(this,'radio_INTONACI')}></input>
                  <em>Intonaci</em>
                </label>
                <label className="mansarda_implants_electric">
                  <input type="checkbox" checked={this.state.radio_ELETTRICO} onChange={this.toggleFormData.bind(this,'radio_ELETTRICO')}></input>
                  <em>Impianto elettico</em>
                </label>
                <label className="mansarda_implants_water">
                  <input type="checkbox" checked={this.state.radio_IDRICO} onChange={this.toggleFormData.bind(this,'radio_IDRICO')}></input>
                  <em>Impianto idrico</em>
                </label>
                <label className="mansarda_implants_heat">
                  <input type="checkbox" checked={this.state.radio_RISCALDAMENTO} onChange={this.toggleFormData.bind(this,'radio_RISCALDAMENTO')}></input>
                  <em>Impianto riscaldamento</em>
                </label>
                <label className="mansarda_implants_cold">
                  <input type="checkbox" checked={this.state.radio_CONDIZIONAMENTO} onChange={this.toggleFormData.bind(this,'radio_CONDIZIONAMENTO')}></input>
                  <em>Impianto condizionamento</em>
                </label>
                <label className="mansarda_implants_alarm">
                  <input type="checkbox" checked={this.state.radio_ALLARME} onChange={this.toggleFormData.bind(this,'radio_ALLARME')}></input>
                  <em>Sistema allarme</em>
                </label>
                <label className="mansarda_implants_bathroom">
                  <input type="checkbox" checked={this.state["exc_WC-PADRONALE"]} onChange={this.toggleFormData.bind(this,'exc_WC-PADRONALE')}></input>
                  <em>Rifacimento bagno</em>
                </label>

              </div>

            </div>

            <div className="mansarda_navigation">
              <button className="mansarda_navigation_prev" onClick={this.setStepIndex.bind(this,-1)}>{this.strings.indietro}</button>
              <button className="mansarda_navigation_next" onClick={this.setStepIndex.bind(this,1)}>{this.strings.avanti}</button>
            </div>

            <div className="mansarda_dots">
              {this.steps.map(function(step, i){
                return <button key={i} className={i == this.state.stepIndex ? 'active' : 'inactive'} onClick={this.setStepIndexAbsolute.bind(this,i)}></button>
              }.bind(this))}
            </div>


          </div>

          <div className="mansarda_step mansarda_info mansarda_step6">

            <div className="mansarda_half">
              <h2>Gli ultimi ritocchi:<br />{"seleziona l'altezza media del soffitto"}</h2>

              <img className="mansarda_info_heightimg" src="images/height.svg" />

              <select className="mansarda_info_height" value={this.state["m-lev"]} onChange={this.setFormDataFromSelect.bind(this,"m-lev")}>
                <option value="0">Fino a 2.30 metri</option>
                <option value="1">Fino a 2.70 metri</option>
                <option value="2">Più di 2.70 metri</option>
              </select>

            </div>

            <div className="mansarda_half2">
              <h2>Devi realizzare una scala<br />per accedere alla mansarda?</h2>

              <img className="mansarda_info_stairsimg" src="images/stairs.svg" />

              <select className="mansarda_info_stairs" value={this.state["m-scala"]} onChange={this.setFormDataFromSelect.bind(this,"m-scala")}>
                <option value="0">Nessuna scala</option>
                <option value="1">Una scala in legno</option>
                <option value="2">Una scala in ferro</option>
              </select>

            </div>

            <div className="mansarda_navigation">
              <button className="mansarda_navigation_prev" onClick={this.setStepIndex.bind(this,-1)}>{this.strings.indietro}</button>
              <button className="mansarda_navigation_next" onClick={this.setStepResults}>Risultati</button>
            </div>

            <div className="mansarda_dots">
              {this.steps.map(function(step, i){
                return <button key={i} className={i == this.state.stepIndex ? 'active' : 'inactive'} onClick={this.setStepIndexAbsolute.bind(this,i)}></button>
              }.bind(this))}
            </div>


          </div>


          <div className="mansarda_step mansarda_results mansarda_step7">

            <div className="mansarda_half">
              <h2> Ecco i risultati calcolati per te. <br /><span className="mansarda_results_restart" onClick={this.setStepRestart}>Ricomincia</span> o <span className="mansarda_results_send" onClick={this.openPopup.bind(this,'send')}>spedisci via email</span></h2>

              <div className="mansarda_results_price">
                <h3>costo al mq</h3>
                <big>{resultPriceMQ}</big>
              </div>
              <div className="mansarda_results_total">
                <h3>costo totale</h3>
                <big>{resultPriceTotal}</big>
              </div>

              <div className="mansarda_results_statistics">
                <h3>Statistiche sul costo</h3>
                <nav>
                  <p className={"bar" + resultStats.levels.muratura}>Muratura <span className="mansarda_results_statistics_bar"></span></p>
                  <p className={"bar" + resultStats.levels.elettrico}>Impianto elettrico <span className="mansarda_results_statistics_bar"></span></p>
                  <p className={"bar" + resultStats.levels.clima}>Clima <span className="mansarda_results_statistics_bar"></span></p>
                  <p className={"bar" + resultStats.levels.wc}>WC <span className="mansarda_results_statistics_bar"></span></p>
                  <p className={"bar" + resultStats.levels.pavimenti}>Pavimenti <span className="mansarda_results_statistics_bar"></span></p>
                  <p className={"bar" + resultStats.levels.infissi}>Infissi <span className="mansarda_results_statistics_bar"></span></p>
                  <p className={"bar" + resultStats.levels.altro}>Altro <span className="mansarda_results_statistics_bar"></span></p>
                </nav>
              </div>

              <div className="mansarda_disclaimer">I prezzi possono essere sovra-stimati o sotto-stimati. Leggi le <a target="_blank" href="http://www.mansarda.it/condizioni-del-servizio-preventivo-mansarda/">condizioni del servizio </a> per ulteriori informazioni</div>

            </div>

            <div className={"mansarda_half2" + " mansarda_results_section_"+this.state.qualitySection}>

              <h2>Scenari sulla <br/> qualità dell’investimento</h2>

              <div className="mansarda_results_quality mansarda_results_step_intro">
                <div className="mansarda_results_intro">
                  <p>Sulla base dei dati inseriti, abbiamo quattro differenti scenari di investimento che consentono di giungere a ritorni e prestazioni migliori. Clicca “procedi” per verificare le opportunità che abbiamo
                  analizzato per te</p>
                  <button onClick={this.setSectionResult.bind(this,'change')}>procedi</button>
                  
                 </div>
              </div>

              <div className="mansarda_results_quality mansarda_results_step_final">
                <div className="mansarda_results_quality_text">
                <h3>Scenario selezionato: <span className="textRed">{qualityTitle}</span></h3>
                  <p>{qualityText}</p>
                </div>
                <big> <button onClick={this.setSectionResult.bind(this,'change')}>cambia scenario</button></big>
              </div>

              <div className="mansarda_results_tips mansarda_results_step_final">
                <h3>Risparmio annuo: € {this.state.qualitySave}</h3>
                <h3>Il nuovo costo totale comprende:</h3>
                <h4>Energia</h4>
                {this.state.qualityTips.map(function(tip, i){
                  return (
                    <p key={i}>{tip.label}<em className="textRed">€ {tip.prezzo}</em></p>
                  );
                }.bind(this))}
                <h4 className="mansarda_results_light">Luminosità e comfort</h4>
                <p>{resultStats.light.tip} <em> {resultStats.light.price != 0 ? "€ " + resultStats.light.price : "" }</em></p>
              </div>

              <div className="mansarda_results_quality mansarda_results_step_change">
                
                <div className="manmansarda_results_change_option">
                  <div className="mansarda_results_change_input">
                   <input type="checkbox" checked={this.state.quality < 1 ? true : false} onChange={this.setQuality.bind(this,0)}></input>
                  </div>
                  <div className="mansarda_results_change_label">
                    <h3>{this.qualityStrings[0].title}</h3>
                    <p>{this.qualityStrings[0].text}</p>
                  </div>
                </div>

                <div className="manmansarda_results_change_option">
                  <div className="mansarda_results_change_input">
                   <input type="checkbox" checked={this.state.quality == 1 ? true : false} onChange={this.setQuality.bind(this,1)}></input>
                  </div>
                  <div className="mansarda_results_change_label">
                    <h3>{this.qualityStrings[1].title}</h3>
                    <p>{this.qualityStrings[1].text}</p>
                  </div>
                </div>

                <div className="manmansarda_results_change_option">
                  <div className="mansarda_results_change_input">
                   <input type="checkbox" checked={this.state.quality == 2 ? true : false} onChange={this.setQuality.bind(this,2)}></input>
                  </div>
                  <div className="mansarda_results_change_label">
                    <h3>{this.qualityStrings[2].title}</h3>
                    <p>{this.qualityStrings[2].text}</p>
                  </div>
                </div>

                <div className="manmansarda_results_change_option">
                  <div className="mansarda_results_change_input">
                   <input type="checkbox" checked={this.state.quality == 3 ? true : false} onChange={this.setQuality.bind(this,3)}></input>
                  </div>
                  <div className="mansarda_results_change_label">
                    <h3>{this.qualityStrings[3].title}</h3>
                    <p>{this.qualityStrings[3].text}</p>
                  </div>
                </div>

              </div>

            </div>

          </div>

        </div>

      </div>
    );
  }

});

ReactDOM.render(
  <Mansarda />,
  document.getElementById('mansarda_app')
);