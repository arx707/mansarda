
window.locations = {

  getProvincie: function(){

    var province = [
      {id: "21",  id_regione: "15", name: "Agrigento"},
      {id: "22",  id_regione: "12", name: "Alessandria"},
      {id: "23",  id_regione: "10", name: "Ancona"},
      {id: "24",  id_regione: "19", name: "Aosta"},
      {id: "25",  id_regione: "16", name: "Arezzo"},
      {id: "26",  id_regione: "10", name: "Ascoli Piceno"},
      {id: "27",  id_regione: "12", name: "Asti"},
      {id: "28",  id_regione: "4", name: "Avellino"},
      {id: "29",  id_regione: "13", name: "Bari"},
      {id: "30",  id_regione: "20", name: "Belluno"},
      {id: "31",  id_regione: "4", name: "Benevento"},
      {id: "32",  id_regione: "9", name: "Bergamo"},
      {id: "33",  id_regione: "12", name: "Biella"},
      {id: "34",  id_regione: "5", name: "Bologna"},
      {id: "35",  id_regione: "17", name: "Bolzano"},
      {id: "36",  id_regione: "9", name: "Brescia"},
      {id: "37",  id_regione: "13", name: "Brindisi"},
      {id: "38",  id_regione: "14", name: "Cagliari"},
      {id: "39",  id_regione: "15", name: "Caltanissetta"},
      {id: "40",  id_regione: "11", name: "Campobasso"},
      {id: "41",  id_regione: "14", name: "Carbonia-Iglesias"},
      {id: "42",  id_regione: "4", name: "Caserta"},
      {id: "43",  id_regione: "15", name: "Catania"},
      {id: "44",  id_regione: "3", name: "Catanzaro"},
      {id: "45",  id_regione: "1", name: "Chieti"},
      {id: "46",  id_regione: "9", name: "Como"},
      {id: "47",  id_regione: "3", name: "Cosenza"},
      {id: "48",  id_regione: "9", name: "Cremona"},
      {id: "49",  id_regione: "3", name: "Crotone"},
      {id: "50",  id_regione: "12", name: "Cuneo"},
      {id: "51",  id_regione: "15", name: "Enna"},
      {id: "52",  id_regione: "5", name: "Ferrara"},
      {id: "53",  id_regione: "16", name: "Firenze"},
      {id: "54",  id_regione: "13", name: "Foggia"},
      {id: "55",  id_regione: "5", name: "Forli Cesena"},
      {id: "56",  id_regione: "7", name: "Frosinone"},
      {id: "57",  id_regione: "8", name: "Genova"},
      {id: "58",  id_regione: "6", name: "Gorizia"},
      {id: "59",  id_regione: "16", name: "Grosseto"},
      {id: "60",  id_regione: "8", name: "Imperia"},
      {id: "61",  id_regione: "11", name: "Isernia"},
      {id: "62",  id_regione: "8", name: "La Spezia"},
      {id: "63",  id_regione: "1", name: "L'Aquila"},
      {id: "64",  id_regione: "7", name: "Latina"},
      {id: "65",  id_regione: "13", name: "Lecce"},
      {id: "66",  id_regione: "9", name: "Lecco"},
      {id: "67",  id_regione: "16", name: "Livorno"},
      {id: "68",  id_regione: "9", name: "Lodi"},
      {id: "69",  id_regione: "16", name: "Lucca"},
      {id: "70",  id_regione: "10", name: "Macerata"},
      {id: "71",  id_regione: "9", name: "Mantova"},
      {id: "72",  id_regione: "16", name: "Massa-Carrara"},
      {id: "73",  id_regione: "2", name: "Matera"},
      {id: "74",  id_regione: "15", name: "Messina"},
      {id: "75",  id_regione: "9", name: "Milano"},
      {id: "76",  id_regione: "5", name: "Modena"},
      {id: "77",  id_regione: "4", name: "Napoli"},
      {id: "78",  id_regione: "12", name: "Novara"},
      {id: "79",  id_regione: "14", name: "Nuoro"},
      {id: "80",  id_regione: "14", name: "Olbia-Tempio"},
      {id: "81",  id_regione: "14", name: "Oristano"},
      {id: "82",  id_regione: "20", name: "Padova"},
      {id: "83",  id_regione: "15", name: "Palermo"},
      {id: "84",  id_regione: "5", name: "Parma"},
      {id: "85",  id_regione: "9", name: "Pavia"},
      {id: "86",  id_regione: "18", name: "Perugia"},
      {id: "87",  id_regione: "10", name: "Pesaro e Urbino"},
      {id: "88",  id_regione: "1", name: "Pescara"},
      {id: "89",  id_regione: "5", name: "Piacenza"},
      {id: "90",  id_regione: "16", name: "Pisa"},
      {id: "92",  id_regione: "6", name: "Pordenone"},
      {id: "93",  id_regione: "2", name: "Potenza"},
      {id: "94",  id_regione: "16", name: "Prato"},
      {id: "95",  id_regione: "15", name: "Ragusa"},
      {id: "96",  id_regione: "5", name: "Ravenna"},
      {id: "97",  id_regione: "3", name: "Reggio Calabria"},
      {id: "98",  id_regione: "5", name: "Reggio Emilia"},
      {id: "99",  id_regione: "7", name: "Rieti"},
      {id: "100",  id_regione: "5", name: "Rimini"},
      {id: "101", id_regione: "7", name: "Roma"},
      {id: "102", id_regione: "20", name: "Rovigo"},
      {id: "103", id_regione: "4", name: "Salerno"},
      {id: "104", id_regione: "14", name: "Medio Campidano"},
      {id: "105", id_regione: "14", name: "Sassari"},
      {id: "106", id_regione: "8", name: "Savona"},
      {id: "107", id_regione: "16", name: "Siena"},
      {id: "108", id_regione: "15", name: "Siracusa"},
      {id: "109", id_regione: "9", name: "Sondrio"},
      {id: "110", id_regione: "13", name: "Taranto"},
      {id: "111", id_regione: "1", name: "Teramo"},
      {id: "112", id_regione: "18", name: "Terni"},
      {id: "113", id_regione: "12", name: "Torino"},
      {id: "114", id_regione: "14", name: "Ogliastra"},
      {id: "115", id_regione: "15", name: "Trapani"},
      {id: "116", id_regione: "17", name: "Trento"},
      {id: "117", id_regione: "20", name: "Treviso"},
      {id: "118", id_regione: "6", name: "Trieste"},
      {id: "119", id_regione: "6", name: "Udine"},
      {id: "120", id_regione: "9", name: "Varese"},
      {id: "121", id_regione: "20", name: "Venezia"},
      {id: "122", id_regione: "12", name: "Verbano-Cusio-Ossola"},
      {id: "123", id_regione: "12", name: "Vercelli"},
      {id: "124", id_regione: "20", name: "Verona"},
      {id: "125", id_regione: "3", name: "Vibo Valentia"},
      {id: "126", id_regione: "20", name: "Vicenza"},
      {id: "127", id_regione: "7", name: "Viterbo"},
      {id: "0", id_regione: "0", name: "Undefined"}
    ];

    return province;

  },

  getIDRegione: function(provincia_id){

    var provincie = window.locations.getProvincie();
    var id_regione = null;

    for (var i = 0; i < provincie.length; i++) {
      var p= provincie[i];
      if(p.id == provincia_id){
        id_regione = p.id_regione;
        break;
      }
    };

    return id_regione;

  }

};

var exports = module.exports = {}
exports.locations = locations;
