<?php

$url = 'http://www.lavorincasa.it/velux.php';

$test = '{"idr":"4","type-g":"medium","affaccio":"2","copertura":"2","m-lev":"1","rinforzo":"2","m-scala":"1","mq":"200","provincia":"77",
"ub":{"comune":"1","zone":"2","centro":"2","ztl":"2","carico":"2","isole":"2","piano":"2"},"level":"medium"}';

if (!isset($_GET['dati'])) {
    die(); // Don't do anything if we don't have a URL to work with
}

$dati = base64_encode($_GET['dati']);

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,"act=GREEN&dati=".$dati);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = curl_exec ($ch);

curl_close ($ch);

echo $server_output;

?>
