<?php 

if ( !isset($_GET['dati']) || !isset($_GET['email'])  ) {
    die();
}

$data = json_decode($_GET['dati']);
$tips = json_decode($_GET['tips']);

$to = $_GET['email'];

$subject = 'Calcolo costi della mansarda';

$headers = "From: info@mansarda.it\r\n";
$headers .= "Reply-To: info@mansarda.it\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

$message = '<html><body style="font-family:Verdana,sans-serif;font-weight:300;">';
$message .= '<h1 style="color:#333">Ecco il calcolo dei costi della mansarda:</h1>';
$message .= '<h2 style="color:#999">Prezzo totale: €'. $data->total.'</h2>';
$message .= '<h2 style="color:#999">Prezzo al mq: €'. $data->mq.'</h2>';
if(count($tips) >= 1){
  $message .= '<hr>';
  $message .= '<h2 style="color:#999">ENERGIA</h2>';
  foreach ($tips as $key => $tip) {
    $message .= '<p style="color:#999">'.$tip->label.': €'. $tip->prezzo .'</p>';
  }  
}
if($data->light->tip != ""){
  $message .= '<hr>';
  $message .= '<h2 style="color:#999">LUMINOSITÀ E COMFORT</h2>';
  $message .= '<p style="color:#999">'.$data->light->tip .': €' . $data->light->price . '</p>';
}
  $message .= '<br><br><br><small style="color:#999">I prezzi possono essere sovra-stimati o sotto-stimati. Leggi le <a target="_blank" href="http://www.mansarda.it/condizioni-del-servizio-preventivo-mansarda/">condizioni del servizio </a> per ulteriori informazioni</small>';

$message .= '</body></html>';

if (mail($to, $subject, $message, $headers) ){
  echo "sent";
}


?>